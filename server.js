var express = require('express'),
    port = 8080,
    app = express(),
    request = require('request');

app.get('/', function (req, res) {
    request('http://hello:8080/version', function(error, response, body) {
        var resJson = JSON.parse(response.body);
        res.send('Hello World is running version ' + resJson.version);
    });
});

app.listen(port);
console.log('Running on http://0.0.0.0:' + port);
